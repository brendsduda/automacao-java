package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utils.ScenarioUtils;

import java.util.HashMap;

public class Hooks {

    @Before
    public void before(Scenario scenario){
        ScenarioUtils.add(scenario);
        HashMap<Object, Object> headers = new HashMap<>();
        HashMap<Object, Object> params = new HashMap<>();
    }

    @After
    public void after() {
        ScenarioUtils.remove();
    }
}
